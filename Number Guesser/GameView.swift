//
//  GameView.swift
//  Number Guesser
//
//  Created by Luke Chambers on 9/21/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI

enum AlertType {
    case win, lose
}

struct GameView: View {
    @ObservedObject var game: Game
    
    @State private var guess = ""
    @State private var showAlert = false
    @State private var alertType = AlertType.win
    
    var body: some View {
        Form {
            Section {
                Text("Guess a number between 1 and \(game.maxNumber).")
                    .font(.headline)
                
                TextField("Guess \(game.currentGuess)/\(game.guesses)", text: $guess)
                    .keyboardType(.numberPad)
                
                Button(action: {
                    let guessResult = self.game.tryGuess(of: Int(self.guess) ?? 0)
                    
                    switch guessResult {
                    case .correct:
                        self.alertType = .win
                        self.showAlert = true
                    case .incorrect:
                        self.guess = ""
                    case .gameOver:
                        self.alertType = .lose
                        self.showAlert = true
                    }
                }) {
                    Text("Make Guess")
                }
                .alert(isPresented: $showAlert) {
                    switch alertType {
                    case .win:
                        return Alert(title: Text("You Won"), message: Text("It took you \(game.currentGuess) tries to guess the number \(game.number)."))
                    case .lose:
                        return Alert(title: Text("You Lost"), message: Text("The number was \(game.number)."))
                    }
                }
            }
            
            Section(header: Text("Previous Guesses")) {
                ForEach(game.previousGuesses.reversed(), id: \.id) { previousGuess in
                    VStack(alignment: .leading) {
                        Text("\(previousGuess.guess)")
                            .font(.headline)
                        Text(previousGuess.result == .low ? "Too Low" : "Too High")
                            .font(.caption)
                    }
                }
            }
        }
        .navigationBarTitle(Text("Game"), displayMode: .inline)
    }
}

struct GameView_Previews: PreviewProvider {
    static var previews: some View {
        GameView(game: Game(maxNumber: 100, guesses: 10))
    }
}
