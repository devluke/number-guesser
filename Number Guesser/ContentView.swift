//
//  ContentView.swift
//  Number Guesser
//
//  Created by Luke Chambers on 9/21/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    let difficultyLevels = ["Easy", "Normal", "Hard", "Extreme"]
    let maxNumbers = [50, 100, 1000, 5000]
    let guessOptions = [5, 10, 15, 20, 25, 30]
    
    @State private var difficulty = 1
    @State private var guesses = 1
    @State private var showGame = false
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Difficulty")) {
                    Picker("Difficulty", selection: $difficulty) {
                        ForEach(0 ..< difficultyLevels.count) { level in
                            Text(self.difficultyLevels[level])
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
                
                Section(header: Text("Guesses")) {
                    Picker("Guesses", selection: $guesses) {
                        ForEach(0 ..< guessOptions.count) { option in
                            Text("\(self.guessOptions[option])")
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
                
                Section(header: Text("Start Game")) {
                    VStack(alignment: .leading) {
                        Text("1-\(maxNumbers[difficulty])")
                            .font(.headline)
                        Text("\(guessOptions[guesses]) Guesses")
                            .font(.caption)
                    }
                    
                    Button(action: {
                        self.showGame = true
                    }) {
                        Text("Start")
                    }
                    .sheet(isPresented: $showGame) {
                        GameView(game: Game(maxNumber: self.maxNumbers[self.difficulty], guesses: self.guessOptions[self.guesses]))
                    }
                }
            }
            .navigationBarTitle("New Game")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
