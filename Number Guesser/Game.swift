//
//  Game.swift
//  Number Guesser
//
//  Created by Luke Chambers on 9/22/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation

class Game: ObservableObject {
    var maxNumber, guesses, number: Int
    
    @Published var currentGuess = 1
    @Published var previousGuesses = [PreviousGuess]()
    
    init(maxNumber: Int, guesses: Int) {
        self.maxNumber = maxNumber
        self.guesses = guesses
        
        number = Int.random(in: 1...maxNumber)
    }
    
    func tryGuess(of guess: Int) -> GuessResult {
        if guess == number {
            return .correct
        } else {
            if currentGuess + 1 > guesses {
                return .gameOver
            } else {
                currentGuess += 1
                
                let previousGuess = PreviousGuess(guess: guess, result: guess < number ? .low : .high)
                previousGuesses.append(previousGuess)
                
                return .incorrect
            }
        }
    }
}

struct PreviousGuess {
    let id = UUID()
    let guess: Int
    let result: PreviousGuessType
}

enum GuessResult {
    case correct, incorrect, gameOver
}

enum PreviousGuessType {
    case low, high
}
